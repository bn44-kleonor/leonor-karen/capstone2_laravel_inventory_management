<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'PageController@welcome');

Route::post('/search', "PageController@search");

Route::get("/ownerview/inviteelist", "PageController@inviteelist");

//unang pagrereferan upon logging in
Route::group(["middleware" => "auth"], function () {
	//for LOGGED IN
	// Route::get("/approval", "HomeController@approval")->name("approval");
	Route::get('/home', 'HomeController@index')->name('home');

	// Route::middleware(["approved"])->group(function(){
	// 	//for logged in and approved user
	// 	Route::get('/home', 'HomeController@index')->name('home');
	// });
	// Route::get("/confirmation/{id}", "PageController@confirmation");

	// Route::get('/events', 'EventController@index')->middleware("auth");
	//if ADMIN and naka-login
	Route::group(["middleware" => ["isAdmin", "auth"]], function()
	{

		// Route:get("/events", "EventController@index");
		Route::get("/users", "UserController@users");

		Route::get("/users", "UserController@users");

		// Admin - all about EVENTS page
		Route::get("/events", "EventController@index");
		Route::post('/events', 'EventController@store');
		Route::delete("/events/{id}", "EventController@destroy");
		Route::get("/restore/{id}", "EventController@restore");
		Route::get("/events/{id}/edit", "EventController@edit");
		Route::get("/events/{id}/show", "EventController@show");
		Route::put("/events/{id}", "EventController@update");
		Route::get("/events/create", "EventController@create");

		// Admin - all about OWNERS page
		Route::post('/owners', 'OwnerController@store');
		Route::get("/owners", "OwnerController@index");
		Route::delete("/owners/{id}", "OwnerController@destroy");
		Route::get("/owners/restore/{id}", "OwnerController@restore");
		Route::get("/owners/{id}/edit", "OwnerController@edit");
		Route::get("/owners/{id}/show", "OwnerController@show");
		Route::put("/owners/{id}", "OwnerController@update");
		Route::get("/owners/create", "OwnerController@create");

		// Route::get("users/{id}/approve", "UserController@approve")->name("approve.user");
	});

		// Owner - all about OwnerVIEW page
		Route::post('/ownerview/{id}', 'OwnerviewController@store');
		Route::get("/ownerview/{id}", "OwnerviewController@index");
		Route::delete("/ownerview/{id}", "OwnerviewController@destroy");
		Route::get("/ownerview/restore/{id}", "OwnerviewController@restore");
		Route::get("/ownerview/{id}/edit", "OwnerviewController@edit");
		Route::get("/ownerview/{id}/show", "OwnerviewController@show");
		Route::put("/ownerview/{id}", "OwnerviewController@update");
		Route::get("/ownerview/create", "OwnerviewController@create");
		// Route::get("/ownerview/inviteelist", "OwnerviewController@inviteelist");

});