@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Hello Admin! You are logged in!
                </div>
            </div>
        </div>
    </div>

    <!-- PRODUCT CHART -->
    @include("admin.partials.product_chart")

</div>

@endsection


@section("script")

{!! $chart->script() !!}

@endsection