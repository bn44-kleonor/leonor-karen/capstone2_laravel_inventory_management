@extends("layouts.app")
@section("content")

<div class="row">
	<div class="col">
		<div class="display-4">{{ $title }}</div>
	</div>
</div>

<form method="POST" action="/owners/{{ $owner->id }}">
	@csrf
	{{ method_field("PUT") }}
	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<!-- Name --> 
					<div class="form-group">
						<label for="first_name">
							First Name:
						</label>
						<input type="text" name="first_name" id="first_name" value="{{ $owner->first_name }}" class="form-control" readonly>
					</div>
					<div class="form-group">
						<label for="last_name">
							Last Name:
						</label>
						<input type="text" name="last_name" id="last_name" value="{{ $owner->last_name }}" class="form-control" readonly>
					</div>
					<!-- Email -->
					<div class="form-group">
						<label for="email">
							Email:
						</label>
						<input type="text" name="email" id="email" value="{{ $owner->email }}" class="form-control" readonly>
					</div>
					<!-- Address -->
					<div class="form-group">
						<label for="address">
							Address:
						</label>
						<input type="text" name="address" id="address" value="{{ $owner->address }}" class="form-control" readonly>
					</div>
				</div>
				<div class="card-footer">
	                <a class="btn btn-warning mr-1" href="/owners/{{ $owner->id }}/edit">
	                	Edit
	                </a>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection