<!-- Footer -->
<footer class="page-footer font-small pt-4 bg-dark">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">R S V P</h5>
        <!-- <p>Here you can use rows and columns to organize your footer content.</p> --> 

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 blue">© 2018 Copyright:
    <!-- <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a> -->
    <a href="#"> Karen Leonor </a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->