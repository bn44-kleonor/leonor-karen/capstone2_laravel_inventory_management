@extends("layouts.app")
@section("content")
	<!-- PRODUCTS IN TABLE -->
	<div class="row">
		<div class="col">
			<div class="display-4">{{ $title }}</div>
		</div>
	</div>
	<div class="row">
			<div class="col">
				<table class="table">
				  <thead class="thead-dark">
				    <tr>
				      <th scope="col" width="25%">#</th>
				      <th scope="col" width="25%">Name</th>
				      <th scope="col" width="25%">Stock</th>
				      <th scope="col" width="25%">Price</th>
				      <th scope="col" width="25%">Action</th>
				    </tr>
				  </thead>
				  <tbody>
					<?php $i = 1; ?>
				  	@foreach($products as $product)
				    <tr>
				      <th scope="row"><?php echo $i++; ?></th>
				      <td>{{ $product->name }}</td>
				      <td>{{ $product->stock }}</td>
				      <td>$ {{ $product->price }}</td>
			          <td>
			            <div class="d-flex flex-row">
			                <!-- READ -->
			                <a href="#" class="btn btn-primary mr-1">	
			                	View
			                </a>
			                <!-- UPDATE -->
			                <a class="btn btn-warning mr-1" href="/products/{{ $product->id }}/edit">
			                	Edit
			                </a>
							<!-------------- DELETE------------ -->
							<!-- DELETE button (easy version) -->
			                <!-- <form method="POST" action="/products/{{ $product->id }}">
			                	@csrf
			                	{{ method_field("DELETE") }}
			                	<button class="btn btn-danger mr-1">Delete</button>
			                </form> -->

							<!-- DELETE 2 (not easy (moderate version) - with modal) -->
							<!-- Button trigger modal -->
							<button type="button" class="btn btn-danger btn-delete-product" data-toggle="modal" data-target="#delete_product_modal" data-id="{{$product->id}}" data-name="{{$product->name}}">
							  Delete
							</button>
							<!-- Modal -->
							<div class="modal fade" id="delete_product_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        Do you want to delete <span id="delete_product_name"></span> ?
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        <form method="POST" id="delete_modal_form">
							        	@csrf
										{{ method_field("DELETE") }}
							        	<button class="btn btn-danger">Delete</button>
							        </form>
							      </div>
							    </div>
							  </div>
							</div>
							<!--------------END DELETE------------ -->

			            </div>
			          </td>
				    </tr>
				    @endforeach
				  </tbody>
				</table>
			</div>
	</div>
	<div class="row">
        <div class="col">
            <a href="#" class="btn btn-success">Add Product</a>
        </div>
    </div>
    <div>
    	<div class="col d-flex">
    		<div class="mx-auto">
    			{{ $products->links() }}
    		</div>
    	</div>
    </div>

@endsection