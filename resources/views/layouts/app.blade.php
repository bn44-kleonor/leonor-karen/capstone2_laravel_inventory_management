<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootswatch -->
    <!-- <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cyborg/bootstrap.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="../../../public/css/bootstrap.css"> --> 
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- PAYPAL -->
    <script src="https://www.paypal.com/sdk/js?client-id=AYcehnmgTA3gZWnmNABVzSBuC2_uZGbjKdnbvNdmNI6XVXd5ocsozaS4oaMwHZyK3uU75w8GWkCgy8dc"></script>

    <!-- CHART JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- FontAwsome -->
    <script src="https://kit.fontawesome.com/ac74f518bc.js" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:900|Muli&display=swap" rel="stylesheet">

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> -->

    <!-- Boostart.com -->
    <!-- <meta charset="utf-8"> -->
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
    <!-- <meta name="description" content=""> -->
    <!-- <meta name="author" content=""> -->

    <!-- <title>Freelancer - Start Bootstrap Theme</title> -->

    <!-- Custom fonts for this theme -->
    <!-- <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css"> -->

    <!-- Theme CSS -->
    <!-- <link href="css/freelancer.min.css" rel="stylesheet"> -->
    <!-- <link href="css/freelancer.css" rel="stylesheet"> -->


</head>
<body>
    <div id="app">
        @include("partials.navbar")
        @include("partials.alerts")

        <main class="py-4 mb-5">
            <div class="container" id="main-container">
                @yield('content')
            </div>
        </main>

        @include("partials.footer")
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/paypal.js')}}"></script>

    <!-- Boostart scripts -->
    <!-- Bootstrap core JavaScript -->
    <!-- <script src="vendor/jquery/jquery.min.js"></script> -->
    <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <!-- <script src="js/freelancer.min.js"></script> -->

    @yield("script")
</body>
</html>
