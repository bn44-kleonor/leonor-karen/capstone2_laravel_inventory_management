@extends("layouts.app")
@section("content")

<div class="row">
	<div class="col">
		<div class="display-4">{{ $title }}</div>
	</div>
</div>

<form method="POST" action="/owners">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<!-- Name --> 
					<div class="form-group">
						<label for="first_name">
							First Name:
						</label>
						<input type="text" name="first_name" id="first_name" class="form-control">
					</div>
					<div class="form-group">
						<label for="last_name">
							Last Name:
						</label>
						<input type="text" name="last_name" id="last_name" class="form-control">
					</div>
					<!-- Email -->
					<div class="form-group">
						<label for="email">
							Email:
						</label>
						<input type="text" name="email" id="email" class="form-control">
					</div>
					<!-- Address -->
					<div class="form-group">
						<label for="address">
							Address:
						</label>
						<input type="text" name="address" id="address" class="form-control">
					</div>
				</div>
				<div class="card-footer">
					<button class="btn btn-warning">
						Create Owner
					</button>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection