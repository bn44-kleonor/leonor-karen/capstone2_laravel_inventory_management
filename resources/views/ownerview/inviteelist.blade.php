@extends("layouts.app")
@section("content")
	<!-- inviteeS IN TABLE --> 
	<div class="row">
		<div class="col">
			<div class="display-4">Hi {{ Auth::user()->username }}, below are {{ $title }}.</div>
		</div>
	</div>
	<div class="row table-responsive">
			<div class="col">
				<table class="table">
				  <thead class="thead-dark">
				    <tr>
				      <th scope="col">#</th>
				      <th scope="col">First Name</th>
				      <th scope="col">Last Name</th>
				      <th scope="col">Email</th>
				      <th scope="col">Contact Number</th>
				      <th scope="col">Response</th>
				      <th scope="col">Action</th>
				    </tr>
				  </thead>
				  <tbody>
					<?php $i = 1; ?>
				  	@foreach($invitees as $invitee)
				    <tr>
				      <td scope="row"><?php echo $i++; ?></td>
				      <td>{{ $invitee->first_name }}</td>
				      <td>{{ $invitee->last_name }}</td>
				      <td>{{ $invitee->email }}</td>
				      <td>{{ $invitee->contact_number }}</td>
				      <td>{{ $invitee->response }}</td>
			          <td>
			            <div class="d-flex flex-row">
			                <!-- READ -->
			                <a class="btn btn-primary mr-1" href="/ownerview/{{ $invitee->id }}/show">	
			                	View
			                </a>
			                <!-- UPDATE -->
			                <a class="btn btn-warning mr-1" href="/ownerview/{{ $invitee->id }}/edit">
			                	Edit
			                </a>
							<!-------------- DELETE------------ -->
							<!-- DELETE button (easy version) -->
			                <!-- <form method="POST" action="/ownerview/{{ $invitee->id }}">
			                	@csrf
			                	{{ method_field("DELETE") }}
			                	<button class="btn btn-danger mr-1">Delete</button>
			                </form> -->

							<!-- DELETE 2 (not easy (moderate version) - with modal) -->
							<!-- Button trigger modal -->
							<button type="button" class="btn btn-danger btn-delete-invitee" data-toggle="modal" data-target="#delete_invitee_modal" data-id="{{$invitee->id}}" data-name="{{$invitee->name}}">
							   X 
							</button>
							<!-- Modal -->
							<div class="modal fade" id="delete_invitee_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        Do you want to delete <span id="delete_invitee_name"></span> ?
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        <form method="POST" id="delete_modal_form">
							        	@csrf
										{{ method_field("DELETE") }}
							        	<button class="btn btn-danger">Delete</button>
							        </form>
							      </div>
							    </div>
							  </div>
							</div>
							<!--------------END DELETE------------ -->

			            </div>
			          </td>
				    </tr>
				    @endforeach
				  </tbody>
				</table>
			</div>
	</div>
	<div class="row">
        <div class="col">
            <a href="#" class="btn btn-success">View Invitee List</a>
            <a class="btn btn-success" href="/ownerview/create">Invite More</a>
        </div>
    </div>
    <div>
    	<div class="col d-flex">
    		<div class="mx-auto">
    		</div>
    	</div>
    </div>

@endsection