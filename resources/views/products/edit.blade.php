@extends("layouts.app")
@section("content")

<div class="row">
	<div class="col">
		<div class="display-4">{{ $title }}</div>
	</div>
</div>

<form method="POST" enctype="multipart/form-data" action="/products/{{ $event->id }}">
	@csrf
	{{ method_field("PUT") }}
	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<!-- Name -->
					<div class="form-group">
						<label for="name">
							Name:
						</label>
						<input type="text" name="name" id="name" value="{{ $event->name }}" class="form-control">
					</div>
					<!-- Occassion -->
					<div class="form-group">
						<label for="name">
							Occasion:
						</label>
						<input type="text" name="occassion" id="occassion" value="{{ $event->occassion }}" class="form-control">
					</div>
					<!-- Place -->
					<div class="form-group">
						<label for="place">
							Place:
						</label>
						<input type="text" name="place" id="place" value="{{ $event->place }}" class="form-control">
					</div>
					<!-- Date -->
					<div class="form-group">
						<label for="date">
							Date:
						</label>
						<input type="text" name="date" id="date" value="{{ $event->date }}" class="form-control">
					</div>
				</div>
				<div class="card-footer">
					<button class="btn btn-warning">
						Save Changes
					</button>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection