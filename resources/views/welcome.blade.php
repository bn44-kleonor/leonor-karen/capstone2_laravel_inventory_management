@extends("layouts.app")
@section("content")
	<!-- Welcome -->
	<!-- <div class="row">
		<div class="col">
			<div class="display-4">Welcome to RSVP</div>
		</div>
	</div> -->

	<!-- COVER IMAGE start --> 
	<section class="mb-5">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 px-0">
					<!-- Image -->
					<div class="cover-image">
					    <!-- <img src="assets/images/pngtree_landscape_city.jpg" class="img-fluid" id="cover-img" alt="sunset city cover photo"> -->
					    <img src="{{asset('img/rsvp_2.jpg')}}" class="img-fluid" id="cover-img" alt="event">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end COVER IMAGE -->


@endsection