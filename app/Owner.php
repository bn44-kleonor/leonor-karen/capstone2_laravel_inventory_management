<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
    use SoftDeletes;
    
    //an Owner hasMany Events
    public function events()
    {
        return $this->hasMany('App\Event');
    }
}
