<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitee extends Model
{
    //an Invitee belongsTo an Event
    public function event()
    {
        return $this->belongsTo('App\Event');
    }
}
