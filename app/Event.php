<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

	//a PRODUCT belongsToMany orders
    public function orders()
    {
        return $this->belongsToMany('App\Order')->withPivot("quantity")->withTimeStamps();
    }

	//an EVENT belongsTo a user
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //an EVENT belongsTo an Owner
    public function owner()
    {
        return $this->belongsTo('App\Owner');
    }

    //an Event hasMany Invitee
    public function invitees()
    {
        return $this->hasMany('App\Invitee');
    }
}
