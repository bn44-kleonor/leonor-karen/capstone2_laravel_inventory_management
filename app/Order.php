<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //an ORDER "belongs to" a single USER (MANY-to-one)
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //an ORDER "has many" status (ONE-to-many)
    public function statuses()
    {
        return $this->hasMany('App\Status');
    }

    //An Order "has many" Payment Mode (one-to-many)
    public function paymentMethod()
    {
        return $this->hasMany('App\PaymentMethod');
    }
    
    //many Order "belongs to many" Products (MANY-to-MANY)
    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot("quantity")->withTimeStamps();
    }
}
