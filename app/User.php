<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'first_name', 'last_name', 'address', 'contact_number', 'role', 'approved _at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //a USER has many orders (one-to-many)
    // 'orders' because of the relationship which is 'many'
    public function orders(){
        return $this->hasMany('App\Order');
    }
    //'App\Order' belongs to the entity (/model) --> file: LaravelEcommerce/app/Order.php;

    //a USER has many events (one-to-many)
    public function events()
    {
        return $this->hasMany('App\Event');
    }

    //a USER has many invitee (one-to-many)
    public function invitees()
    {
        return $this->hasMany('App\Invitee');
    }

}
