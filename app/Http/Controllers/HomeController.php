<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\ProductChart;
use App\Product;
use App\Category;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance. 
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd("fed");
        if(Auth::user()->role == "admin")
        {
            //call a method that will return a value for chart

            // $chart = $this->productChart();
            // $greeting = `Welcome to back $Auth::user()->username`;
            // $greeting = `Welcome to back "Auth::user()->username"`;
            // dd($chart);
            // return view("admin.home", compact("chart"));
            // return view("admin.home", $greeting);
            return view("admin.home");
        }
            return view('owners.home');
    }

    // public function approval()
    // {
    //     //dd(!Auth::user()->approved_at); //null - not approved, not null - meaning approved already by admin
    //     //if(Auth::user()->approved_at)
    //     if(Auth::user()->approved_at || Auth::user()->role == 'admin')
    //     {
    //         //return view("home");
    //         return $this->index();  //return to the 'index' function of this same file
        
    //     }
    //     return view("approval");
    // }

    private function productChart()
    {
        //sample chart
        // $chart = new ProductChart;
        // $chart->labels(["Category 1", "Category 2", "Category 3", "Category 4"]);
        // $chart->dataset("Product count:", "line", [1, 2, 3, 4]);

        //=============================================================
        //1) DB QUERY
        $products = Product::all();
        $products = $products->groupBy("category_id")->map(function($product) {
            return count($product);
        });

        //dd($products);
        //2) INSTANTIATE CHART
        $chart = new ProductChart;

        //3) ASSIGN LABELS
        //$chart->labels($products->keys());  //refactor later to have category_name instead of id
        $labels = [];
        foreach($products->keys() as $key)
        {
            $category = Category::find($key);
            $labels[] = $category->name;
        }
        $chart->labels($labels);


        //4) ASSIGN DATASET
        //types: line, bar, horizontal, doughnut, pie
        $chart->dataset("Product count", "line", $products->values())
            ->dashed([10])
            ->fill(true)
            ->backgroundColor([
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)'
                              ])
            ->color([
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)'
                    ]);

        //5) CUSTOMIZE
        $chart->minimalist(true);   //boolean
        $chart->displayLegend(true);

        $title = "Category of Products";
        $font_size = 18;
        $color = "#6600ff";
        $bold = true;
        $chart->title($title, $font_size, $color, $bold);

        return $chart;
        //return "test";
    }
}
