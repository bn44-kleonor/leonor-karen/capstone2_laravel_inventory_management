<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Event;
use App\Owner;
use App\Invitee;
use App\User;
use Auth;
use Session;

class PageController extends Controller
{
    //index method - returns welcome.blade.php with all products
    public function welcome() 
    {  
        $title = "Events list";
        // $events = Event::orderBy("name", "desc")->paginate(10); 
        
        // $events = DB::table('events')
        //     ->join('owners', 'events.owner_id', '=', 'owners.id')
        //     ->select('events.*', 'owners.first_name as ownerfname', 'owners.last_name as ownerlname')
        //     ->orderBy("name", "desc")
        //     ->get();
            // ->paginate(10);

        // $users = DB::table('users')
        // ->join('contacts', 'users.id', '=', 'contacts.user_id')
        // ->join('orders', 'users.id', '=', 'orders.user_id')
        // ->select('users.*', 'contacts.phone', 'orders.price')
        // ->get();

        $events = Event::orderBy("name", "desc")
            ->paginate(10);

        // dd($events);

        return view("welcome", compact("title", "events"));
    }


    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inviteelist()
    {
        // dd("test");
        // $title = "Your Invitees";
        // $invitees = Invitee::where('invitees.event_id', '=', $id)
        //     ->orderBy("first_name", "desc")
        //     ->get();
        // // dd($id);
        // return view("ownerview.inviteelist", compact("title", "invitees"));

        $title = "Your Invitees";
        $invitees = Invitee::orderBy("first_name", "desc")
            ->get();

        // dd($invitees);

        return view("ownerview.inviteelist", compact("title", "invitees"));

    }


    public function search(Request $request){
        // dd($request->input("search"));
        // dd($request->search);
        $searchKey = $request->search;
        $products = Event::where("name", "like", "%" . $searchKey . "%")->paginate(10);
        // return view("partials.search")->with('products', $products)->render();
        if($products->count() > 0){
            return view("partials.search")->with('products', $products);
        } else {
            return "none";
        }
    }
}
