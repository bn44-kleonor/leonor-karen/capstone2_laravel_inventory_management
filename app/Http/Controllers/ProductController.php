<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Product;
use App\Event;
use App\Owner;
use App\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Products";

        // 1) this displays all products, "excluding" soft-deleted ones
        // $products = Product::orderBy("name")->paginate(3);
        // return view('products.index')->with('products', $products);
    
        //2) to display all products, "including" soft-deleted ones
        // $products = Product::withTrashed()->get();
    
        //3) to display only soft deleted products
        // $products = Product::onlyTrashed()->get();
        // $products = Product::onlyTrashed()->paginate(10);

        // dd($products);
        // return view("products.index", compact('title', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Create Event";
        return view("products.create")->with('title', $title);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd('test');
        $this->validate($request, [
            'name' => 'required',
            'occassion' => 'required',
            'place' => 'required',
            'date' => 'required',
            'owner_id' => 'required'
        ]);
        // dd('test');
        //create new post
        $event = new Event;
        $event->name = $request->input('name');
        $event->occassion = $request->input('occassion');
        $event->place = $request->input('place');
        $event->date = $request->input('date');
        $event->owner_id = $request->input('owner_id');
        $event->save();
        $id = $event->id;

        return redirect("/")->with('success', "$event->name was successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $title = "Edit Event Page";
        $event = DB::table('events')
            ->join('owners', 'events.owner_id', '=', 'owners.id')
            ->select('events.*', 'owners.first_name as ownerfname', 'owners.last_name as ownerlname')
            ->where('events.id', '=', $id)
            ->first();
        
        // dd($event);
        return view("products.show", compact("title", "event"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $title = "Edit Event Page";
        // $event = Event::find($id);
        // return view("products.edit", compact("title", "event"));

        $title = "Edit Event Page";
        $event = DB::table('events')
            ->join('owners', 'events.owner_id', '=', 'owners.id')
            ->select('events.*', 'owners.first_name as ownerfname', 'owners.last_name as ownerlname')
            ->where('events.id', '=', $id)
            ->first();
        
        // dd($event);
        return view("products.edit", compact("title", "event"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd("test");
        $this->validate($request, [
            'name' => 'required',
            "occassion" => "required",
            "place" => "required",
            "date" =>  "required"
            // "first_name" =>  "required",
            // "last_name" =>  "required"
            ]);
        // dd("test");
        $event = Event::find($id);
        $event->name = $request->input("name");
        $event->occassion = $request->input("occassion");
        $event->place = $request->input("place");
        $event->date = $request->input("date");
        // $owner->first_name = $request->input("first_name");
        // $owner->last_name = $request->input("last_name");


        $event->save();
        return redirect()->back()->with("success", "Changes has been saved!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event_name = $event->name;
        $event->delete();
        // return redirect()->back()->with("success", "$product_name has been deleted!");
        return redirect("/")
        ->with("success", "$event_name has been deleted!")
        ->with("undo_url","/restore/$id");
    }

    public function restore($id)
    {
        //get softdeleted products

        $event = Event::onlyTrashed()->where("id", $id)->first();  //get returns a collection
        // dd($event_name);

        $event->restore();
        return back()->with("success", "$event->name has been restored!");
    }
}