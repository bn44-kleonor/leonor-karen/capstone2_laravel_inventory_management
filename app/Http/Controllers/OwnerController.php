<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Event;
use App\Owner;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Event Owner list";

        // $owners = DB::table('events')
        //     ->join('owners', 'events.owner_id', '=', 'owners.id')
        //     ->select('owners.*', 'events.id as event_id', 'events.name as eventname', 'events.occassion', 'events.place', 'events.date')
        //     ->orderBy("owners.first_name", "desc")
        //     ->get();

        $owners = Owner::orderBy("first_name", "desc")
            ->paginate(10);

        // dd($owners);

        return view("owners.index", compact("title", "owners"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Create New Event Owner";
        return view("owners.create")->with('title', $title);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd('test');
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'user_id' => 'required'
        ]);
        // dd('test');
        //create new post
        $owner = new Owner;
        $owner->first_name = $request->input('first_name');
        $owner->last_name = $request->input('last_name');
        $owner->email = $request->input('email');
        $owner->address = $request->input('address');
        $owner->user_id = $request->input('user_id');
        $owner->save();
        $id = $owner->id;
        // dd($owner);

        return redirect("owners")->with('success', "$owner->first_name $owner->last_name was successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $title = "View Event Owner Page";
        // $owner = DB::table('events')
        //     ->join('owners', 'events.owner_id', '=', 'owners.id')
        //     ->select('owners.*', 'events.name as eventname', 'events.occassion', 'events.place', 'events.date')
        //     ->where('owners.id', '=', $id)
        //     ->first();
        

        $owner = Owner::where('owners.id', '=', $id)
                ->orderBy("first_name", "desc")
                ->first();
                // ->paginate(10);

        // $order = Order::where('user_id', $id)->orderBy("created_at", "desc")->first();

        // dd($owner->id);
        return view("owners.show", compact("title", "owner"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $title = "Edit Event Page";
        // $event = Event::find($id);
        // return view("events.edit", compact("title", "event"));

        $title = "Edit Owner Page";
            // $owner = DB::table('events')
            // ->join('owners', 'events.owner_id', '=', 'owners.id')
            // ->select('owners.*', 'events.name as eventname', 'events.occassion', 'events.place', 'events.date')
            // ->where('owners.id', '=', $id)
            // ->first();
        
        // dd($event);
    
        $owner = Owner::where('owners.id', '=', $id)
                ->orderBy("first_name", "desc")
                ->first();
                // ->paginate(10);

        // $order = Order::where('user_id', $id)->orderBy("created_at", "desc")->first();

        // dd($owner->id);
        return view("owners.edit", compact("title", "owner"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd("test");
        $this->validate($request, [
            'first_name' => 'required',
            "last_name" => "required",
            "email" => "required",
            "address" =>  "required"
            ]);
        // dd("test");
        $owner = Owner::find($id);
        $owner->first_name = $request->input("first_name");
        $owner->last_name = $request->input("last_name");
        $owner->email = $request->input("email");
        $owner->address = $request->input("address");

        $owner->save();
        return redirect()->back()->with("success", "Changes has been saved!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $owner = Owner::find($id);
        $ownerfname = $owner->first_name;
        $ownerlname = $owner->last_name;
        $owner->delete();
        // return redirect()->back()->with("success", "$product_name has been deleted!");
        return redirect("/owners")
        ->with("success", "$ownerfname $ownerfname has been deleted!")
        ->with("undo_url","/owners/restore/$id");
    }

    public function restore($id)
    {
        //get softdeleted products

        $owner = Owner::onlyTrashed()->where("id", $id)->first();  //get returns a collection
        // dd($event_name);

        $owner->restore();
        return back()->with("success", "$owner->first_name $owner->last_name has been restored!");
    }
}
