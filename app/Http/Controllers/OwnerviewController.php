<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Event;
use App\Owner;
use App\Invitee;

class OwnerviewController extends Controller
{
    /**
     * Display a listing of the resource. 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // $id = 3;
        $title = "Your Events";
        $events = Event::where('events.owner_id', '=', $id)
            ->orderBy("name", "desc")
            ->get();

        // dd($id);

        return view("ownerview.index", compact("title", "events"));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inviteelist()
    {
        // dd("test");
        // $title = "Your Invitees";
        // $invitees = Invitee::where('invitees.event_id', '=', $id)
        //     ->orderBy("first_name", "desc")
        //     ->get();
        // // dd($id);
        // return view("ownerview.inviteelist", compact("title", "invitees"));

        $title = "Your Invitees";
        $invitees = Invitee::orderBy("first_name", "desc")
            ->get();

        dd($invitees);

        return view("ownerview.inviteelist", compact("title", "invitees"));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $id = 2;
        $title = "Your Events";
        $events = Event::where('events.owner_id', '=', $id)
            ->orderBy("name", "desc")
            ->first();

        // dd($events);

        return view("ownerview.index", compact("title", "events"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
