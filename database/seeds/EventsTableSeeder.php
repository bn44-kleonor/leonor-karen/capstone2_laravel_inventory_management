<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('events')->delete();
        
        \DB::table('events')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Little Ricky\'s 1st Birthday',
                'occassion' => 'Birthday',
                'place' => '123 St. Barangay ABC, Makati City',
                'date' => 'October 23, 2019',
                'status' => 'upcoming',
                'owner_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Desi\'s Elementary Graduation',
                'occassion' => 'Graduation',
                'place' => '234 St. Barangay DEF, Pasay City',
                'date' => 'March 24, 2019',
                'status' => 'upcoming',
                'owner_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Vivian\'s Bridal Shower',
                'occassion' => 'Others',
                'place' => '345 St. Barangay HIJ, Quezon City',
                'date' => 'April 25, 2019',
                'status' => 'archived',
                'owner_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'William\'s Championship Celebration',
                'occassion' => 'Others',
                'place' => '456 St. Barangay HIJ, Pasig City',
                'date' => 'May 26, 2019',
                'status' => 'archived',
                'owner_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Little Lucy\'s 5st Birthday',
                'occassion' => 'Birthday',
                'place' => '123 St. Barangay ABC, Makati City',
                'date' => 'January 23, 2020',
                'status' => 'archived',
                'owner_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Lucille\'s High School Graduation',
                'occassion' => 'Graduation',
                'place' => '234 St. Barangay DEF, Pasay City',
                'date' => 'March 24, 2019',
                'status' => 'archived',
                'owner_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Ricardo Family\'s Christmas Party',
                'occassion' => 'Others',
                'place' => '345 St. Barangay HIJ, Quezon City',
                'date' => 'December 25, 2019',
                'status' => 'upcoming',
                'owner_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Mert\'z Aparment Tenants New Year Celebration',
                'occassion' => 'Others',
                'place' => '456 St. Barangay HIJ, Pasig City',
                'date' => 'January 01, 2020',
                'status' => 'upcoming',
                'owner_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}