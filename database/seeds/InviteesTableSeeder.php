<?php

use Illuminate\Database\Seeder;

class InviteesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('invitees')->delete();
        
        \DB::table('invitees')->insert(array (
            0 => 
            array (
                'id' => 1,
                'first_name' => 'Juan',
                'last_name' => 'dela Cruz',
                'email' => 'juan@gmail.com',
                'contact_number' => 12345,
                'response' => 'yes',
                'event_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'first_name' => 'Matt',
                'last_name' => 'dela Cruz',
                'email' => 'matt@gmail.com',
                'contact_number' => 23456,
                'response' => 'no',
                'event_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'first_name' => 'Simon',
                'last_name' => 'dela Cruz',
                'email' => 'simon@gmail.com',
                'contact_number' => 34567,
                'response' => 'maybe',
                'event_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'first_name' => 'Gregorio',
                'last_name' => 'dela Cruz',
                'email' => 'gregorio@gmail.com',
                'contact_number' => 45678,
                'response' => '',
                'event_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'first_name' => 'Juan2',
                'last_name' => 'dela Cruz',
                'email' => 'juan2@gmail.com',
                'contact_number' => 12345,
                'response' => 'yes',
                'event_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'first_name' => 'Matt2',
                'last_name' => 'dela Cruz',
                'email' => 'matt2@gmail.com',
                'contact_number' => 23456,
                'response' => 'no',
                'event_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'first_name' => 'Simon2',
                'last_name' => 'dela Cruz',
                'email' => 'simon2@gmail.com',
                'contact_number' => 34567,
                'response' => 'maybe',
                'event_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'first_name' => 'Gregorio2',
                'last_name' => 'dela Cruz',
                'email' => 'gregorio2@gmail.com',
                'contact_number' => 45678,
                'response' => '',
                'event_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'first_name' => 'Juan3',
                'last_name' => 'dela Cruz',
                'email' => 'juan3@gmail.com',
                'contact_number' => 12345,
                'response' => 'yes',
                'event_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'first_name' => 'Matt3',
                'last_name' => 'dela Cruz',
                'email' => 'matt3@gmail.com',
                'contact_number' => 23456,
                'response' => 'no',
                'event_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'first_name' => 'Simon3',
                'last_name' => 'dela Cruz3',
                'email' => 'simon3@gmail.com',
                'contact_number' => 34567,
                'response' => 'maybe',
                'event_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'first_name' => 'Gregorio3',
                'last_name' => 'dela Cruz',
                'email' => 'gregorio3@gmail.com',
                'contact_number' => 45678,
                'response' => '',
                'event_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'first_name' => 'Juan4',
                'last_name' => 'dela Cruz',
                'email' => 'juan4@gmail.com',
                'contact_number' => 12345,
                'response' => 'yes',
                'event_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'first_name' => 'Matt4',
                'last_name' => 'dela Cruz',
                'email' => 'matt4@gmail.com',
                'contact_number' => 23456,
                'response' => 'no',
                'event_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'first_name' => 'Simon4',
                'last_name' => 'dela Cruz',
                'email' => 'simon4@gmail.com',
                'contact_number' => 34567,
                'response' => 'maybe',
                'event_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'first_name' => 'Gregorio4',
                'last_name' => 'dela Cruz',
                'email' => 'gregorio4@gmail.com',
                'contact_number' => 45678,
                'response' => '',
                'event_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'first_name' => 'Juan5',
                'last_name' => 'dela Cruz',
                'email' => 'juan5@gmail.com',
                'contact_number' => 12345,
                'response' => 'yes',
                'event_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'first_name' => 'Matt5',
                'last_name' => 'dela Cruz',
                'email' => 'matt5@gmail.com',
                'contact_number' => 23456,
                'response' => 'no',
                'event_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'first_name' => 'Simon5',
                'last_name' => 'dela Cruz',
                'email' => 'simon5@gmail.com',
                'contact_number' => 34567,
                'response' => 'maybe',
                'event_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'first_name' => 'Gregorio5',
                'last_name' => 'dela Cruz',
                'email' => 'gregorio5@gmail.com',
                'contact_number' => 45678,
                'response' => '',
                'event_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}