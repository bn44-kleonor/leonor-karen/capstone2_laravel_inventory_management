const alert_container = document.querySelector("#alert_container");

function removeAlert()
{
	setTimeout(function(){
		alert_container.innerHTML = "";
	//3000 milliseconds or 3 seconds
	}, 3000);
}

function displaySuccessMessage(content)
{
	let alert_success = document.createElement("div");
	alert_success.setAttribute("class", "alert alert-success");
	alert_success.setAttribute("id", "alert_success");
	alert_success.textContent = content;
	// let alert_container = document.querySelector("#alert_container");
	alert_container.append(alert_success);
	removeAlert();
}

function displayErrorMessage(content)
{
	//id found at: 	<div class="alert alert-danger" id="alert_error">
	let alert_error = document.createElement("div");
	alert_error.setAttribute("class", "alert alert-danger");
	alert_error.setAttribute("id", "alert_error");
	alert_error.textContent = content;
	// let alert_container = document.querySelector("#alert_container");
	alert_container.append(alert_error);
	removeAlert();
}

document.addEventListener("click", function(e) 
{
	// console.log(e.target.classList.contains("btn-delete-event"));
	//====DELETE "Event"
	if(e.target.classList.contains("btn-delete-event")) 
	{
		// alert("hello");
		let button = e.target;
		//get event_id
		let event_id = button.dataset.id;
		//get event_name
		let event_name = button.dataset.name;
		//put event_name inside span
		let modal_text = document.querySelector("#delete_event_name");
		modal_text.innerHTML = event_name;
		//get the modal's form
		let modal_form = document.querySelector("#delete_modal_form");
		//set action attribute to modal form
		modal_form.setAttribute("action", `/events/${event_id}`);
	}

	//====DELETE "Owner"
	if(e.target.classList.contains("btn-delete-owner")) 
	{
		// alert("hello");
		let button = e.target;
		//get owner_id
		let owner_id = button.dataset.id;
		//get owner_name
		let owner_name = button.dataset.name;
		//put owner_name inside span
		let modal_text = document.querySelector("#delete_owner_name");
		modal_text.innerHTML = owner_name;
		//get the modal's form
		let modal_form = document.querySelector("#delete_modal_form");
		//set action attribute to modal form
		modal_form.setAttribute("action", `/owners/${owner_id}`);
	}

	// ======== TRANSFER event TO BE DELETE TO MODAL
	if(e.target.classList.contains("btn-show-delete-modal"))
	{
		// alert("test");
		let button = e.target;
		let event_name = button.dataset.name;
		let event_id = button.dataset.id;

		let modal_form = document.querySelector("#delete_cart_event_form");
		let modal_button = modal_form.lastElementChild;
		// console.log(modal_button);
		modal_button.setAttribute("data-id", event_id);
		modal_button.setAttribute("data-name", event_name);

		let modal_text = document.querySelector("#delete_cart_event_name");
		modal_text.innerHTML = event_name;
	}
})

document.addEventListener("change", function(e) 
{
	//refactor this (e.keyCode)
	//e.preventDefault(); 

	//============== SEARCH
	let inputSearch = document.querySelector("#search");
	inputSearch.addEventListener("keydown", function(e)
	{
		console.log(e.keyCode);
		if(e.keyCode == 13) {
			e.preventDefault();
			let searchKey = inputSearch.value;
			// alert(searchKey);

			let form = document.querySelector("#search-form");

			fetch(`/search`, {
				method: "POST",
				body: new FormData(form)
			})
			.then(function(response){
				return response.text();
			})
			.then(function(data_from_fetch){
				if(data_from_fetch != "none"){
					let container = document.querySelector("#main-container");
					container.innerHTML = data_from_fetch;
				} else {
					let content = `Sorry ${searchKey} didn't match any of our events. :( `;
					displayErrorMessage(content);
				}
			})
			.catch(function(error){
				console.log("Request failed", error);
			})
		}
	})
})
