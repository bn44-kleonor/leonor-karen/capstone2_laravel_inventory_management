if(document.querySelector("#paypal-button-container"))
{
 
    let total = document.querySelector("#total");
    let user_id = document.querySelector("#user_id");
 
    paypal.Buttons({
    createOrder: function(data, actions) {
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: total.value
 
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // Capture the funds from the transaction
      return actions.order.capture().then(function(details) {
        // Show a success message to your buyer
        alert('Transaction completed by ' + details.payer.name.given_name);
 
        //fetch\
             if(details.status == "COMPLETED")
            {
                let form = document.querySelector("#user-details-form");
                let formData = new FormData(form);
                formData.append('paypal_id', data.orderID);
 
                return fetch(`/confirmation/${user_id.value}`, {
                    method: "POST",
                    credentials: "same-origin",
                    body: formData
                })
                .then(function(response) {
                    return response.text();
                })
                .then(function(data_from_fetch) {
                    //console.log(data_from_fetch);
                    
                    //render
                    let container = document.querySelector("#main-container");
                    container.innerHTML = data_from_fetch;
                })
                .catch(function(error) {
                    console.log("Response error", error);
                })
            }
      });
    }
  }).render('#paypal-button-container');
}